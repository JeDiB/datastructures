/*
 * myBTree.cc - Copyright (c) 2014 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* My implementation of a no balanced binary tree data structure.
 * TODO : templatize.
 */

#include "myBTree.h"

namespace DataStructures
{

/* class BTNode */
BTNode::BTNode(const value_type& value, BTNode* daddy, BTNode* left, BTNode* right)
  : _value(value)
  , _daddy(daddy)
  , _left(left)
  , _right(right)
{}

BTNode::~BTNode() 
{}

/* class BTree */
BTree::BTree()
  : _root(NULL)
  , _size(0)
{}

BTree::BTree(const value_type& value)
  : _root(new BTNode(value))
  , _size(0)
{}

BTree::BTree(const BTree& tree)
  : _root(NULL)
  , _size(tree._size)
{
  if(tree._root != NULL) {
    _root = _copy(*tree._root);
  }
}

BTNode* BTree::_copy(const BTNode& node) {
  BTNode* twin = new BTNode(node._value);
  if(node._left != NULL) {
    twin->_left = _copy(*node._left);
    twin->_left->_daddy = twin;
  }
  if(node._right != NULL) {
   twin->_right =  _copy(*node._right);
    twin->_right->_daddy = twin;
  }
  return twin;
}

BTree::~BTree()
{
  clear();
}

BTree& BTree::operator=(const BTree& tree) {
  // self-assignment care
  if(this != &tree) {
    clear();
    if(tree._root != NULL) {
      _root = _copy(*tree._root);
    }
  }

  return *this;
}

void BTree::clear() {
  if(_root != NULL) {
    _clear(_root);
    _root = NULL;
    _size = 0;
  }
}

void BTree::_clear(BTNode* node) {
  if(node->_left != NULL) {
    _clear(node->_left);
  }
  if(node->_right != NULL) {
    _clear(node->_right);
  }
  delete node;
  node = NULL;
}

std::pair<BTNode*, bool> BTree::insert(const BTree::value_type& value) {
  BTNode* node = NULL;
  bool success = false;

  if(_root != NULL) {
    success = _insert(*_root, &node, value);
  }
  else {
    success = true;
    node = new BTNode(value);
    _root = node;
  }
  if(success) {
    ++_size;
  }

  return std::make_pair(node, success);
}

bool BTree::_insert(BTNode& position, BTNode** element, const BTree::value_type& value) {
  BTNode* node = NULL;
  if(std::less<BTNode::value_type>()(value, position._value)) {
    node = position._left;
    if(node != NULL) {
      return _insert(*node, element, value);
    }
    else {
      *element = new BTNode(value);
      position._left = *element;
      (*element)->_daddy = &position;
      return true;
    }
  }
  else if(std::less<BTNode::value_type>()(position._value, value)) {
    node = position._right;
    if(node != NULL) {
      return _insert(*node, element, value);
    }
    else {
      *element = new BTNode(value);
      position._right = *element;
     (*element)->_daddy = &position;
      return true;
    }
  }
  else { // equivalent
    *element = &position;
    return false;
  }
}

void BTree::erase(const value_type& value) {
  BTNode* node = _root;
  char fromSide = 0; // 0 if root, 1 if node is a left child, 2 if node is a right child
  static size_t alternate = 0;

  while(node != NULL) {
    if(!(std::less<BTNode::value_type>()(node->_value,value)) && !(std::less<BTNode::value_type>()(value,node->_value))) {
      if(node->_left != NULL) {
        if(node->_right != NULL) { // 2 children
          // Let's find the node in the left subtree the most on the right
          // or the node in the right subtree the most on the left (alternate between both)
          BTNode* target = NULL;
          if((alternate & 1) == 0) {
            target = node->_left;
            fromSide = 1;
            while(target->_right != NULL) {
              target = target->_right;
              fromSide = 2;
            }
          }
          else {
            target = node->_right;
            fromSide = 2;
            while(target->_left != NULL) {
              target = target->_left;
              fromSide = 1;
            }
          }
          std::swap(target->_value, node->_value);
          node = target; // After swapping nodes, the node to erase can only have one child so the next iteration will take a different condition branch.
        }
        else { // Node has left child only
          switch(fromSide) {
            case 0: _root = node->_left; break; // is Root (all privileges ;) )
            case 1: node->_daddy->_left = node->_left; break;
            case 2: node->_daddy->_right = node->_left; break;
            default: break;
          }
          delete node;
          node = NULL;
        }
      }
      else if(node->_right != NULL) { // Node has right child only
        switch(fromSide) {
          case 0: _root = node->_right; break; // is Root (I am gRoot ;) )
          case 1: node->_daddy->_left = node->_right; break;
          case 2: node->_daddy->_right = node->_right; break;
          default: break;
        }
        delete node;
        node = NULL;
      }
      else { // Node is a leaf
        switch(fromSide) {
          case 0: _root = NULL; break; // is Root (no more jokes)
          case 1: node->_daddy->_left = NULL; break;
          case 2: node->_daddy->_right = NULL; break;
          default: break;
        }
        delete node;
        node = NULL;
      }
      if(node == NULL) {
        ++alternate;
        --_size;
        return;
      }
    }
    else if(std::less<BTNode::value_type>()(node->_value,value)) {
      node = node->_right;
      fromSide = 2;
    }
    else {
      node = node->_left;
      fromSide = 1;
    }
  }
}

BTree::value_type& BTree::root() {
  if(_root != NULL) {
    return _root->_value;
  }
}

const BTree::value_type& BTree::root() const {
  if(_root != NULL) {
    return _root->_value;
  }
}

void BTree::swap(BTree& tree) {
  std::swap(_root, tree._root);
  std::swap(_size, tree._size);
}

size_t BTree::size() const {
  return _size;
}

void BTree::printPre() const {
  if(_root != NULL) {
    _printPre(*_root);
  }
}

void BTree::printPost() const {
  if(_root != NULL) {
    _printPost(*_root);
  }
}

void BTree::_printPre(const BTNode& node) const {
  static size_t num = 0;
  for(int i; i < num; i++)
    std::cout << ">";

  std::cout << node.getValue() << std::endl;
  ++num;
  if(node._left != NULL) {
    _printPre(*node._left);
  }
  if(node._right != NULL) {
    _printPre(*node._right);
  }
  --num;
}

void BTree::_printPost(const BTNode& node) const {
  static size_t num = 0;
  ++num;
  if(node._left != NULL) {
    _printPost(*node._left);
  }
  if(node._right != NULL) {
    _printPost(*node._right);
  }
  --num;
  for(int i; i < num; i++)
    std::cout << ">";

  std::cout << node.getValue() << std::endl;
}

};

//------------------------------------------------------------------------------
// End of File
//------------------------------------------------------------------------------
