/*
 * myHeap.tpp - Copyright (c) 2014 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

template<typename T, typename Compare>
Heap<T, Compare>::Heap(const Compare& comp)
  : _comp(comp)
{
}

/*template<typename T, typename Compare> template<typename InputIterator>
void Heap<T, Compare>::_range_init(InputIterator first, InputIterator last, std::input_iterator_tag) {
  for(; first != last; ++first)
  	_data.insert(*first);
}

template<typename T, typename Compare> template<typename InputIterator>
Heap<T, Compare>::Heap(InputIterator first, InputIterator last) {
  typedef typename std::iterator_traits<InputIterator>::iterator_category IterCategory;
  _range_init(first, last, IterCategory());
}*/

template<typename T, typename Compare>
Heap<T, Compare>::~Heap() {
}

template<typename T, typename Compare>
void Heap<T, Compare>::insert(const T& value) {
  _data.push_back(value);
  int son = size() - 1;
  while( _shiftUp(son) ) {
    int dad = (son - 1) / 2;
  	std::swap(_data[son], _data[dad]);
  	son = dad;
  };
}

template<typename T, typename Compare>
bool Heap<T, Compare>::_shiftUp(int pos) {
  if(pos == 0)
  	return false;
  else {
  	int dad = (pos - 1) / 2;
    // Do not handle equivalence ... Should return false if equivalent
    // to avoid useless shifts.
  	if( _comp(_data[pos], _data[dad]) )
  		return false;
  	else {
  		return true;
  	}
  }
}

template<typename T, typename Compare>
bool Heap<T, Compare>::_shiftDown(int pos) {
  if(pos >= size())
  	return false;
  int dad = (pos - 1 ) / 2;
  if( _comp(_data[dad], _data[pos]) )
  	return true;
  return false;
}

template<typename T, typename Compare>
int Heap<T, Compare>::_max(int  lhs, int rhs) {
 return _comp(_data[lhs], _data[rhs]) ? rhs:lhs;
}

template<typename T, typename Compare>
void Heap<T, Compare>::popTop() {
  _data[0] = _data[size()-1];
  _data.pop_back();

  int son = 1;
  int max = _max(son, son + 1);
  if(max != son)
  	son += 1;

  while( _shiftDown(son) ) {
    int dad = (son - 1) / 2;
  	std::swap(_data[son], _data[dad]);
  	son = 2 * son + 1;
  	int max = _max(son, son + 1);
  	if(max != son)
  	son += 1;
  };
}

template<typename T, typename Compare>
void Heap<T, Compare>::print() const {
  cout << "---- Heap(hop) ----" << endl;

  int length = _data.size();
  cout << _data[0] << endl;

  int height = 1;
  for(int i = 1; i < length; ++i) {
  	int h = log(i + 1) / log(2);
  	if( h != height) {
  	  height = h;
  		cout << endl;
  	}
  	cout << _data[i] << ' ';
  }
  cout << endl; 
}

//------------------------------------------------------------------------------
// End of File
//------------------------------------------------------------------------------
