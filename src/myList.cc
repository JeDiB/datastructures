/*
 * myList.cc - Copyright (c) 2014 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* My implementation of a list data structure. Doubly linked List.
 * TODO : templatize, add sort, merge functions
 */

#include "myList.h"

#include <ctime>

namespace DataStructures
{

/* class Node */
Node::Node(const value_type& value, Node* prev, Node* next)
  : _value(value)
  , _prev(prev)
  , _next(next)
{}

Node::~Node() 
{}

/* class List */
List::List()
  : _front(NULL)
  , _back(NULL)
{}

List::List(const List& list)
  : _front(NULL)
  , _back(NULL)
{
  if(list._front != NULL) {
    Node* node = new Node(list._front->_value);
    _front = node;
    for(Node* n = list._front->_next; n != NULL; n = n->_next) {
      node = new Node(n->_value, node);
      node->_prev->_next = node;
    }
    _back = node;
  }
}

List::~List()
{
  clear();
}

List& List::operator=(const List& list) {
  // self-assignment care
  if(this != &list) {
    clear();
    if(list._front != NULL) {
      Node* node = new Node(list._front->_value);
      _front = node;
      for(Node* n = list._front->_next; n != NULL; n = n->_next) {
        node = new Node(n->_value, node);
        node->_prev->_next = node;
      }
      _back = node;
    }
  }

  return *this;
}

void List::clear() {
  if(_front != NULL) {
    for(Node* node = _front->_next; node != NULL; node = node->_next) {
      delete node->_prev;
      node->_prev = NULL;
    }
    delete _back;
    _back = NULL;
    _front = NULL;
  }
}

size_t List::size() const {
  size_t size = 0;
  for(Node* node = _front; node != NULL; node = node->_next, ++size);
  return size;
}

void List::pop_back() {
  if(_front != _back ) { // size > 1
    Node* node = _back->_prev;
    delete _back;
    _back = node;
    node->_next = NULL;
  }
  else if((_front != NULL) && (_front == _back)) { // size == 1
    delete _back;
    _back = NULL;
    _front = NULL;
  }
}

void List::pop_front() {
  if(_front != _back ) { // size > 1
    Node* node = _front->_next;
    delete _front;
    _front = node;
    node->_prev = NULL;
  }
  else if((_front != NULL) && (_front == _back)) { // size == 1
    delete _front;
    _front = NULL;
    _back = NULL;
  }
}

void List::push_back(const value_type& value) {
  Node* node = new Node(value);
  if(_back == NULL) {
    _front = node;
    _back = node;
  }
  else {
    node->_prev = _back;
    _back->_next = node;
    _back = node;
  }
}

void List::push_front(const value_type& value) {
  Node* node = new Node(value);
  if(_front == NULL) {
    _front = node;
    _back = node;
  }
  else {
    node->_next = _front;
    _front->_prev = node;
    _front = node;
  }
}

Node* List::insert(Node* position, const value_type& value) {
  if(position != NULL) {
    // ToDo : handle exceptions if cannot allocate memory
    Node* node = new Node(value);
    node->_prev = position->_prev;
    node->_next = position;
    if(position != _front) {
      position->_prev->_next = node;
    }
    else {
      _front = node;
    }
    position->_prev = node;
    return node;
  }
  else {
    return NULL;
  }
}

// Todo changer pour que ca retourne et donc garder un pointeur sur position next
Node* List::erase(Node* position) {
  if(position == NULL) {
    return NULL;
  }
  else {
    Node* ret = position->_next;
    if(position == _front) {
      if((_front != NULL) && (_front == _back)) {
        delete _front;
        _front = NULL;
        _back = NULL;
      }
      else {
        Node* node = _front->_next;
        delete _front;
        node->_prev = NULL;
        _front = node;
      }
    }
    else if(position == _back) {
      if((_front != NULL) && (_front == _back)) {
        delete _back;
        _back = NULL;
        _front = NULL;
      }
      else {
        Node* node = _back->_prev;
        delete _back;
        node->_next = NULL;
        _back = node;
      }
    }
    else {
      Node* _prev = position->_prev;
      Node* _next = position->_next;
      _prev->_next = _next;
      _next->_prev = _prev;
      delete position;
      position = NULL;
    }
    return ret;
  }
}



List::value_type& List::front() {
  return _front->_value;
}

const List::value_type& List::front() const {
  return _front->_value;
}

List::value_type& List::back() {
  return _back->_value;
}

const List::value_type& List::back() const {
  return _back->_value;
}

Node* List::begin() {
  return _front;
}

const Node* List::begin() const {
  return _front;
}

Node* List::end() {
  return _back;
}

const Node* List::end() const {
  return _back;
}

void List::swap(List& list) {
  std::swap(_front, list._front);
  std::swap(_back, list._back);
}

void List::print() const {
  for(Node* node = _front; node != NULL; node = node->_next) {
    std::cout << node->_value << ' ';
  }
  std::cout << std::endl;
}

};

//------------------------------------------------------------------------------
// End of File
//------------------------------------------------------------------------------
