/*
 * myBTree.h - Copyright (c) 2015 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* My implementation of a no balanced binary tree data structure.
   The tree has unique value (the uniqueness is based on equivalence, not equality)
 */

#ifndef MY_TREE_H
#define MY_TREE_H

#include <cstdlib>
#include <iostream>
#include <functional>

namespace DataStructures
{

class BTree;

class BTNode
{
friend class BTree;
public:
  typedef int value_type;

public:
  explicit BTNode(const value_type& value, BTNode* daddy = NULL ,BTNode* left = NULL, BTNode* right = NULL);
  ~BTNode();

  const value_type& getValue() const {
    return _value;
  }

private: // disable copy
  BTNode(const BTNode& node);
  BTNode& operator=(const BTNode& node);

private: // private data
  value_type _value;
  BTNode* _daddy;
  BTNode* _left;
  BTNode* _right;
};

class BTree
{
public:
  typedef BTNode::value_type value_type;

public:
  explicit BTree();
  explicit BTree(const value_type& value);
  BTree(const BTree& tree);
  ~BTree();

  BTree& operator=(const BTree& tree);

  bool empty() const {
    if(_root != NULL)
      return false;
    else 
      return true;
  }

  size_t size() const;

  void clear();

  std::pair<BTNode*, bool> insert(const value_type& value);
  void erase(const value_type& value); 

  value_type& root();
  const value_type& root() const;

  void swap(BTree& tree);

  void printPre() const;
  void printPost() const;

private: // private functions
  bool _insert(BTNode& position, BTNode** element, const value_type& value);
  void _clear(BTNode* node);
  void _printPre(const BTNode& node) const;
  void _printPost(const BTNode& node) const;
  BTNode* _copy(const BTNode& node); 

private: // private data
    BTNode* _root;
    size_t _size;
};

};

#endif

//------------------------------------------------------------------------------
// End of File
//------------------------------------------------------------------------------
