/*
 * myHeap.h - Copyright (c) 2014 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* My implementation of a heap data structure based on the underlying vector
 * STL container so i do not have to deal with memory allocations. These are
 * done by the default allocator of std::vector in the free-store :-)
 * By default, the heap is a max-heap.
 */

#ifndef MY_HEAP_H
#define MY_HEAP_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <iterator>
#include <functional>

using std::vector;
using std::iterator;
using std::cout;
using std::cin;
using std::endl;


template<typename T, typename Compare = std::less<T> >
class Heap
{
public: // public API

  /* Ctor & Dtor */
  Heap(const Compare& comp = Compare());
  //template<typename InputIterator>
  //Heap(InputIterator first, InputIterator last);

  ~Heap();

  /* Classic Heap Ops */

  /* Return the top
   * Complexity O(1)
   */
  T& findTop() const {
  	return _data[0];
  }

  /* Remove the top from the heap.
   * must keep the invariance (always a heap after the deletion)
   * Complexity O(Log n)
   */
  void popTop();

  /* Insert value in the heap
   * must keep the invariance (always a heap after the insertion)
   * Complexity O(Log n)
   */
  void insert(const T& value); // O(Log n)

  /* Returns size of the heap 
   * Complexity O(1) constant time operation
   */
  size_t size() const { // O(1)
  	return _data.size();
  }
  /* Returns true if heap is empty
   * Complexity O(1) constant time operation
   */
  bool isEmpty() const { // O(1)
  	return size()==0;
  }

  void print() const; // Help visualize.

private: // private functions
  bool _shiftUp(int pos);
  bool _shiftDown(int pos);
   /* Returns the index of the max of elements at lhs and rhs index.
    */
  int _max(int lhs, int rhs);
  //template<typename InputIterator>
  //void _range_init(InputIterator first, InputIterator last, std::input_iterator_tag);

private: // private data
  vector<T> _data;
  Compare _comp;
};

#include "myHeap.tpp"
#endif

//------------------------------------------------------------------------------
// End of File
//------------------------------------------------------------------------------
