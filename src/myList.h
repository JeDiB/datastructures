/*
 * myList.h - Copyright (c) 2014 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* My implementation of a list data structure. Doubly linked List.
 */

#ifndef MY_LIST_H
#define MY_LIST_H

#include <cstdlib>
#include <iostream>

namespace DataStructures
{

class List;

class Node
{
friend class List;
public:
  typedef int value_type;

public:
  explicit Node(const value_type& value, Node* prev = NULL, Node* next = NULL);
  ~Node();

  const value_type& getValue() const {
    return _value;
  }

private: // disable copy
  Node(const Node& node);
  Node& operator=(const Node& node);

private: // private data
  value_type _value;
  Node* _prev;
  Node* _next;
};

class List
{
public:
  typedef Node::value_type value_type;

public:
  explicit List();
  List(const List& list);
  ~List();

  List& operator=(const List& list);

  bool empty() const {
    if(_front != NULL)
      return false;
    else 
      return true;
  }

  size_t size() const;

  void clear();

  void pop_back();
  void pop_front();
  void push_back(const value_type& value);
  void push_front(const value_type& value);

  Node* insert(Node* position, const value_type& value);
  Node* erase(Node* position); 

  int& front();
  const int& front() const;
  int& back();
  const int& back() const;

  Node* begin();
  const Node* begin() const;
  Node* end();
  const Node* end() const;

  void swap(List& list);

  void print() const;

private: // private data
    Node* _front;
    Node* _back;
};

};

#endif

//------------------------------------------------------------------------------
// End of File
//------------------------------------------------------------------------------
