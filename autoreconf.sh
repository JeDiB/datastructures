#!/bin/sh
#
# autoreconf.sh - Copyright (c) 2014 -
#
# This file is part of DataStrcutures
#
# This file is not free software: you cannot redistribute it and/or modify
# it without the explicit permission of the owners. All rights reserved.
#

AUTORECONF=`which autoreconf`
if test -z "${AUTORECONF}"; then
    echo "*** autoreconf was not found, please install it ***"
    exit 1
fi

"${AUTORECONF}" -v -i -f || exit $?
