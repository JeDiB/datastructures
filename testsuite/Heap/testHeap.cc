/*
 * testHeap.cc - Copyright (c) 2014 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Enjoy ! 
 */

#include "myHeap.h"

#include <iostream>
#include <cstdlib>


using std::istream;
using std::ostream;


class Widget
{
public:
  Widget(int id = 0, int price = 0) 
  : _id(id)
  , _price(price)
  {}


  int getId() const { return _id; }
  int getPrice() const { return _price; }

  int& getId() { return _id; }
  int& getPrice() { return _price; }

  int getprint() const {
    return _price; 
  }

private:
  int _id;
  int _price;
};

istream& operator>>(istream& stream, Widget& widget)
{
  cout << "Id? ";
  stream >> widget.getId();
  cout << "Price? ";
  stream >>  widget.getPrice();
  return stream;
}

ostream& operator<<(ostream& stream, const Widget& widget)
{
  stream << widget.getprint();
  return stream;
}


class compWidgetPrice : public std::binary_function<Widget, Widget, bool>
{
public:
  bool operator()(const Widget& lhs, const Widget& rhs) const {
    return lhs.getPrice() < rhs.getPrice();
  }
};

int main(int argc, char **argv)
{
  cout << "Fun with Heaps" << endl;
  
  Heap<Widget, compWidgetPrice> wh((compWidgetPrice()));
  char choice;
  wh.insert(Widget(1,10));
  wh.insert(Widget(1,20));
  wh.insert(Widget(1,30));
  wh.insert(Widget(1,40));
  wh.insert(Widget(1,25));
  wh.insert(Widget(1,35));
  wh.insert(Widget(1,38));
  wh.insert(Widget(1,99));
  Widget widget;
  while(1) {
    cout << "(P)rint Heap. (I)nsert, (R)emove one Widget to/from heap. Your call ? ";
    cin >> choice;
    switch(choice) {
      case 'p':
      case 'P':
        wh.print();
        break;
      case 'i':
      case 'I':
        cin >> widget;
        wh.insert(widget); 
        break;
      case 'r':
      case 'R':
        wh.popTop();
        break;
      default:
        cout << "Wrong choice !" << endl;
        break;
    }
  } 

  return EXIT_SUCCESS;
}
