/*
 * testBTree.cc - Copyright (c) 2015 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "myBTree.h"

#include <cstdlib>

static int myrandom(int i) {
  return rand()%i;
}

int main(void)
{
  using namespace DataStructures;
  using namespace std;

  srand(unsigned(time(NULL)));


//  BTree mytree;
//  for(int i = 0; i < 20; ++i) {
//    int n = myrandom(100);
//    mytree.insert(n);
//    cout << "Insert " << n << endl;
//  }
//  mytree.printPre();
//
//  for(int i = 0; i < 10; ++i)
//    mytree.erase(50+i);
//
//  cout << endl;
//
//  mytree.printPre();


  int data[18] = { 50, 30, 80, 10, 45, 70, 100, 5, 25, 48, 60, 75, 46, 47, 55, 73, 78, 77};

  BTree tree;
  for(int i = 0; i < 18; ++i) {
    tree.insert(data[i]);
  }
  tree.printPre();
  cout << "Root is " << tree.root() << "\tSize is " << tree.size() << endl;

  cout << endl;

  std::pair<BTNode*, bool> pair = tree.insert(66);
  cout << "Insert 66 ? " << std::boolalpha << pair.second << endl;
  cout << "Value insérée : " << pair.first->getValue() << endl;

  pair = tree.insert(66);
  cout << "Insert 66 ? " << std::boolalpha << pair.second << endl;

  cout << endl;
  tree.printPre();
  cout << "Root is " << tree.root() << "\tSize is " << tree.size() << endl;

  return EXIT_SUCCESS;
}

//------------------------------------------------------------------------------
// End of File
//------------------------------------------------------------------------------
