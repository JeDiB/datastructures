/*
 * testList.cc - Copyright (c) 2014 -
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Have fun */

#include "myList.h"

#include <cstdlib>

static int myrandom(int i) {
  return rand()%i;
}

int main(void)
{
  using namespace DataStructures;
  using namespace std;

  srand(unsigned(time(NULL)));

  List mylist;
  for(int i = 0; i < 10; ++i) {
    mylist.push_back(myrandom(1000));
  }
  mylist.print();

//  List mylist2;
//  for(int i = 0; i < 20; ++i) {
//    mylist2.push_back(myrandom(1000));
//  }
//  mylist2.print();
//
//  cout << "Before swap magic trick ! Size liste 1 = " << mylist.size() << "\tSize liste 2 = " << mylist2.size() << endl << endl;
//
//  mylist.swap(mylist2);
//
//  cout << "After Swap magic trick ! Size liste 1 = " << mylist.size() << "\tSize liste 2 = " << mylist2.size() << endl;
//  mylist.print();
//  mylist2.print();

  List copy(mylist);
  copy.print();

  cout << "liste initiale [via reference] Front => " << mylist.front() << "\tBack => " << mylist.back() << "\tSize " << mylist.size() << endl;
  cout << "liste initiale[via pointeur] Front => " << mylist.begin()->getValue() << "\tBack => " << mylist.end()->getValue() << "\tSize " << mylist.size() << endl;

  cout << "liste copiee [via reference] Front => " << copy.front() << "\tBack => " << copy.back() << "\tSize " << copy.size() << endl;
  cout << "liste copiee [via pointeur] Front => " << copy.begin()->getValue() << "\tBack => " << copy.end()->getValue() << "\tSize " << copy.size() << endl;

  
  Node* node = mylist.begin();
  mylist.insert(mylist.begin(), 10);
  mylist.insert(mylist.begin(), 9);
  mylist.insert(mylist.begin(), 8);
  mylist.insert(node, 11);
  node = mylist.insert(mylist.end(), 999);
  node = mylist.insert(node, 998);
  mylist.print();
  cout << "liste initiale [via reference] Front => " << mylist.front() << "\tBack => " << mylist.back() << "\tSize " << mylist.size() << endl;
  
  copy = mylist;
  copy.print();
  cout << "liste copiee [via reference] Front => " << copy.front() << "\tBack => " << copy.back() << "\tSize " << copy.size() << endl;

  copy = copy;
  node = mylist.erase(node);
  mylist.print();
  cout << "Apres le erase, le noeud retourne est " << node->getValue() << endl;

  copy.clear();
  cout << "liste copiee\tSize " << copy.size() << endl;

  return EXIT_SUCCESS;
}

//------------------------------------------------------------------------------
// End of File
//------------------------------------------------------------------------------
